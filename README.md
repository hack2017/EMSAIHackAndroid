# Android Basic sample app

This app is a starting point for an user wanting to use the hackathon backend on Android. A few components have been implemented for a developer to pick up and extend, or use as reference.

## Modules

This project uses an additional module for networking to simplify API calls, check the app module's build.gradle file for more details on how those are added:

- com.android.volley - Easy networking module; you volley some data to the server, it volleys back a response

## Components in the project

- Kitchen Sink (links to all function calls)
- Sentiment Analysis (provides a sentiment reading based on the perceived tone of text)
- Optical Character Recognition (attempts to read any text in the provided image)
- Speech-to-Text (converts audio to text)
- License Plate Recognition (specifically looks for license plates and provides their value)
- Tweet Analysis (provides several Twitter related functions)

## Structure

- activities/ - All screens users can interact with, paired with layouts in /res
- networking/ - Handles all app networking logic
- networking/ControlPanel.java - Interface for making calls to the API server


