package com.telus.sample.hackathonboilerplate.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.telus.sample.hackathonboilerplate.AudioPlayer;
import com.telus.sample.hackathonboilerplate.R;
import com.telus.sample.hackathonboilerplate.Util;
import com.telus.sample.hackathonboilerplate.networking.ControlPanel;

import org.json.JSONObject;

import java.io.InputStream;

public class SpeechToTextAnalysis extends AppCompatActivity {

    private String base64EncodedSpeech;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speech_to_text_analysis);

        InputStream audioFileInputStream = getResources().openRawResource(R.raw.cycle006);
        base64EncodedSpeech = Util.convertRawInputStreamToBase64(audioFileInputStream);
        Log.d("Audio size", base64EncodedSpeech.length() + "");

        findViewById(R.id.playAudio).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AudioPlayer().play(getApplication(), R.raw.cycle006);
            }
        });

        final TextView sttAnalyzed = (TextView) findViewById(R.id.sttAnalyzed);

        findViewById(R.id.submitSttButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControlPanel.doSpeechToText(base64EncodedSpeech, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        sttAnalyzed.setText(response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        sttAnalyzed.setText("ERROR: " + error.getMessage());
                    }
                });
            }
        });
    }

}
