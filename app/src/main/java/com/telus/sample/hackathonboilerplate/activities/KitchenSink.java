package com.telus.sample.hackathonboilerplate.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.telus.sample.hackathonboilerplate.R;
import com.telus.sample.hackathonboilerplate.networking.RequestQueueOven;

public class KitchenSink extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kitchen_sink);

        RequestQueueOven.instantiateRequestQueue(this);

        findViewById(R.id.sentimentAnalysisTestButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SentimentAnalysis.class);
            }
        });


        findViewById(R.id.ocrTestButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(OcrAnalysis.class);
            }
        });


        findViewById(R.id.twitterTestButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(TweetAnalysis.class);
            }
        });


        findViewById(R.id.licensePlateTestButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(AlprAnalysis.class);
            }
        });


        findViewById(R.id.speechToTextTestButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(SpeechToTextAnalysis.class);
            }
        });

    }


    private void startActivity(Class activityClass) {
        startActivity(new Intent(getApplicationContext(), activityClass));
    }

}
