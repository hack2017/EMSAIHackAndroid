package com.telus.sample.hackathonboilerplate;

import android.util.Base64;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by garett on 2016-06-15.
 */
public class Util {

    public static final String TOKEN = "token";
    public static final String AUTH_TOKEN = "9gMbELAXLHLTE5mghQxHw9KqsNeEvsQmzTNmzFE7DcdELCNEuYuUpyMp4AnhcVr2";

    public static final String BASE_URL = "https://hack2017.mbenablers.com/bikes";


    public static String convertRawInputStreamToBase64(InputStream inputStream){
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int i;
            while ((i = inputStream.read(buffer, 0, buffer.length)) > 0){
                byteArrayOutputStream.write(buffer, 0, i);
            }
            byteArrayOutputStream.flush();
            byte [] file = byteArrayOutputStream.toByteArray();
            byteArrayOutputStream.close();
            inputStream.close();

            return new String(Base64.encode(file, Base64.NO_WRAP));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}