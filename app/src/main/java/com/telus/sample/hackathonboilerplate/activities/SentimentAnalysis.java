package com.telus.sample.hackathonboilerplate.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.telus.sample.hackathonboilerplate.R;
import com.telus.sample.hackathonboilerplate.networking.ControlPanel;

import org.json.JSONObject;

public class SentimentAnalysis extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sentiment_analysis);


        final EditText sentimentText = (EditText) findViewById(R.id.sentiment);

        final TextView sentimentAnalysis = (TextView) findViewById(R.id.sentimentAnalyzed);

        findViewById(R.id.submitSAButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControlPanel.doSentimentAnalysis(sentimentText.getText().toString(), new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        sentimentAnalysis.setText(response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        sentimentAnalysis.setText("ERROR: " + error.getMessage());
                    }
                });
            }
        });
    }
}
