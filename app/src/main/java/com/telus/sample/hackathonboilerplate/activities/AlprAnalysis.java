package com.telus.sample.hackathonboilerplate.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.telus.sample.hackathonboilerplate.R;
import com.telus.sample.hackathonboilerplate.Util;
import com.telus.sample.hackathonboilerplate.networking.ControlPanel;

import org.json.JSONObject;

import java.io.InputStream;

public class AlprAnalysis extends AppCompatActivity {

    String imageBase64Encoded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alpr_analysis);

        InputStream imageInputStream = getResources().openRawResource(R.raw.toyota);
        imageBase64Encoded = Util.convertRawInputStreamToBase64(imageInputStream);

        Log.d("Image size ALPR", imageBase64Encoded.length() + "");

        final TextView alprAnalyzed = (TextView) findViewById(R.id.alprAnalyzed);

        findViewById(R.id.submitAlprButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControlPanel.doAutomatedLicensePlateRecognition(imageBase64Encoded, "us", "on", new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        alprAnalyzed.setText(response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        alprAnalyzed.setText("ERROR: " + error.getMessage());
                    }
                });
            }
        });
    }

}
