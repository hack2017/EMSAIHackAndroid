package com.telus.sample.hackathonboilerplate.networking;

import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.telus.sample.hackathonboilerplate.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by garett on 2016-10-18.
 */

public class ControlPanel {


    private static final String TOKEN = Util.TOKEN;
    private static final String AUTH_TOKEN = Util.AUTH_TOKEN;
    private static final String BASE_URL = Util.BASE_URL;

    public static void doSentimentAnalysis(String content, final Response.Listener<JSONObject> responseListener, final Response.ErrorListener errorListener) {

        int requestMethod = Request.Method.POST;
        String RESOURCE_URL = BASE_URL + "/sentiment";

        JSONObject sentimentAnalysisRequest = new JSONObject();
        JSONObject contentJson = new JSONObject();

        try {
            sentimentAnalysisRequest.put(TOKEN, AUTH_TOKEN);
            contentJson.put("content", content);
            sentimentAnalysisRequest.put("document", contentJson);
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError("Failed to create request payload"));
            return;
        }
        Log.d("Output", sentimentAnalysisRequest.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(requestMethod, RESOURCE_URL, sentimentAnalysisRequest, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                responseListener.onResponse(response);
            }
        }, errorListener);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueueOven.getRequestQueue().add(jsonObjectRequest);
    }

    public static void doSpeechToText(String base64EncodedAudio, final Response.Listener<JSONObject> responseListener, final Response.ErrorListener errorListener) {

        doSpeechToText(base64EncodedAudio, null, responseListener, errorListener);
    }

    public static void doSpeechToText(String base64EncodedAudio, JSONObject config, final Response.Listener<JSONObject> responseListener, final Response.ErrorListener errorListener) {

        int requestMethod = Request.Method.POST;
        String RESOURCE_URL = BASE_URL + "/transcript";

        JSONObject speechToTextRequest = new JSONObject();

        try {
            speechToTextRequest.put(TOKEN, AUTH_TOKEN);
            if (config != null) {
                speechToTextRequest.put("config", config);
            } else {
                JSONObject defaultConfig = new JSONObject();
                defaultConfig.put("encoding", "LINEAR16");
                defaultConfig.put("sampleRateHertz", 32000);
                defaultConfig.put("languageCode", "en-GB");
                speechToTextRequest.put("config", defaultConfig);
            }
            speechToTextRequest.put("audio", base64EncodedAudio);
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError("Failed to create request payload"));
            return;
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(requestMethod, RESOURCE_URL, speechToTextRequest, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                responseListener.onResponse(response);
            }
        }, errorListener);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueueOven.getRequestQueue().add(jsonObjectRequest);
    }

    public static void doOpticalCharacterRecognition(String base64EncodedImage, final Response.Listener<JSONObject> responseListener, final Response.ErrorListener errorListener) {

        int requestMethod = Request.Method.POST;
        String RESOURCE_URL = BASE_URL + "/ocr";

        JSONObject opticalCharacterRecognitionRequest = new JSONObject();

        try {
            opticalCharacterRecognitionRequest.put(TOKEN, AUTH_TOKEN);
            opticalCharacterRecognitionRequest.put("image", base64EncodedImage);
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError("Failed to create request payload"));
            return;
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(requestMethod, RESOURCE_URL, opticalCharacterRecognitionRequest, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                responseListener.onResponse(response);
            }
        }, errorListener);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueueOven.getRequestQueue().add(jsonObjectRequest);
    }

    public static void doAutomatedLicensePlateRecognition(String base64EncodedImage, String code, String state, final Response.Listener<JSONObject> responseListener, final Response.ErrorListener errorListener) {

        int requestMethod = Request.Method.POST;
        String RESOURCE_URL = BASE_URL + "/alpr";

        JSONObject automatedLicensePlateRecognitionRequest = new JSONObject();

        try {
            automatedLicensePlateRecognitionRequest.put(TOKEN, AUTH_TOKEN);
            automatedLicensePlateRecognitionRequest.put("image", base64EncodedImage);
            automatedLicensePlateRecognitionRequest.putOpt("code", code);
            automatedLicensePlateRecognitionRequest.putOpt("state", state);
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError("Failed to create request payload"));
            return;
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(requestMethod, RESOURCE_URL, automatedLicensePlateRecognitionRequest, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                responseListener.onResponse(response);
            }
        }, errorListener);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueueOven.getRequestQueue().add(jsonObjectRequest);
    }

    public static void doTweetAnalysis(String path, String screenName, final Response.Listener<JSONObject> responseListener, final Response.ErrorListener errorListener) {

        int requestMethod = Request.Method.POST;
        String RESOURCE_URL = BASE_URL + "/twitter";

        JSONObject tweetAnalysisRequest = new JSONObject();
        JSONObject params = new JSONObject();

        try {
            tweetAnalysisRequest.put(TOKEN, AUTH_TOKEN);
            tweetAnalysisRequest.put("path", path);

            params.put("screen_name", screenName);

            tweetAnalysisRequest.put("params", params);
        } catch (JSONException e) {
            errorListener.onErrorResponse(new VolleyError("Failed to create request payload"));
            return;
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(requestMethod, RESOURCE_URL, tweetAnalysisRequest, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                responseListener.onResponse(response);
            }
        }, errorListener);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(0, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        RequestQueueOven.getRequestQueue().add(jsonObjectRequest);
    }

}
