package com.telus.sample.hackathonboilerplate.networking;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Created by garett on 2016-10-17.
 */

public class RequestQueueOven {

    private static RequestQueue mRequestQueue;

    public static void instantiateRequestQueue(Context context) {
        mRequestQueue = Volley.newRequestQueue(context);
    }

    public static RequestQueue getRequestQueue(){
        return mRequestQueue;
    }
}
