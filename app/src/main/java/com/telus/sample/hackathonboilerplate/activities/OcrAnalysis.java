package com.telus.sample.hackathonboilerplate.activities;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.telus.sample.hackathonboilerplate.R;
import com.telus.sample.hackathonboilerplate.Util;
import com.telus.sample.hackathonboilerplate.networking.ControlPanel;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

public class OcrAnalysis extends AppCompatActivity {

    String imageBase64Encoded ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ocr_analysis);

        InputStream imageInputStream = getResources().openRawResource(R.raw.what_is_artificial_intelligence);
        imageBase64Encoded = Util.convertRawInputStreamToBase64(imageInputStream);
        Log.d("Image size", "" + imageBase64Encoded.length());

        final TextView ocrAnalyzed = (TextView) findViewById(R.id.ocrAnalyzed);

        findViewById(R.id.submitOcrButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControlPanel.doOpticalCharacterRecognition(imageBase64Encoded, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        ocrAnalyzed.setText(response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ocrAnalyzed.setText("ERROR: " + error.getMessage());
                    }
                });
            }
        });
    }
}
