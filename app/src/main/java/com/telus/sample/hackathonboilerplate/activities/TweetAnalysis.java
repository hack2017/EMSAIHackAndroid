package com.telus.sample.hackathonboilerplate.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.telus.sample.hackathonboilerplate.R;
import com.telus.sample.hackathonboilerplate.networking.ControlPanel;

import org.json.JSONObject;

public class TweetAnalysis extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweet_analysis);

        final EditText tweetPathText = (EditText) findViewById(R.id.tweetPath);
        final EditText twitterScreenNameText = (EditText) findViewById(R.id.twitterScreenName);

        final TextView tweetAnalysis = (TextView) findViewById(R.id.tweetAnalyzed);


        findViewById(R.id.submitTweetButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControlPanel.doTweetAnalysis(tweetPathText.getText().toString(), twitterScreenNameText.getText().toString(),  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        tweetAnalysis.setText(response.toString());
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        tweetAnalysis.setText("ERROR: " + error.getMessage());
                    }
                });
            }
        });
    }

}
